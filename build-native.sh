#!/bin/bash

export ARCH=native
export PROJECT_ROOT=`pwd`

mkdir -p ./build/${ARCH}/intr/{obj,moc,ui,rc}/{ModulOSC,test_ModulOSC} 2>&1 > /dev/null

find . -maxdepth 3 -name "Makefile" -exec rm {} \;

qmake-qt4 "DEFINES += I2C_DEBUG"
make $*

# TODO; make qmake handle these!
cp -r ModulOSC_scripts/src/${ARCH}/* ./build/${ARCH}
cp -r PyModulOSC/src/* ./build/${ARCH}
