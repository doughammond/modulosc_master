##   ModulOSC; An open platform for OSC control of analogue devices
##
##   Copyright (C) 2014  Doug Hammond
##
##   This program is free software: you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, either version 3 of the License, or
##   (at your option) any later version.
##
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.
##
##   You should have received a copy of the GNU General Public License
##   along with this program.  If not, see <http://www.gnu.org/licenses/>.

DESTDIR = $(PROJECT_ROOT)/build/$(ARCH)

OBJECTS_DIR = $$DESTDIR/intr/obj/$$TARGET
MOC_DIR = $$DESTDIR/intr/moc/$$TARGET
UI_DIR = $$DESTDIR/intr/ui/$$TARGET
RCC_DIR = $$DESTDIR/intr/rc/$$TARGET
