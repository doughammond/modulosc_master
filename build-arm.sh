#!/bin/bash

export ARCH=arm
export PROJECT_ROOT=`pwd`

mkdir -p ./build/${ARCH}/intr/{obj,moc,ui,rc}/{ModulOSC,test_ModulOSC} 2>&1 > /dev/null

find . -maxdepth 3 -name "Makefile" -exec rm {} \;

export PATH=`cat ../CrossProjects/crosscompile_path`:$PATH
export CROSS_SYSROOT=`cat ../CrossProjects/rpi_sysroot_path`
export TC_SYSROOT=`cat ../CrossProjects/x86_sysroot_path`

export OE_QMAKE_INCDIR_QT=${CROSS_SYSROOT}/usr/include/qtopia
export OE_QMAKE_LIBDIR_QT=${CROSS_SYSROOT}/usr/lib/qtopia

export OE_QMAKE_QMAKE=${TC_SYSROOT}/usr/bin/qmake2
export OE_QMAKE_MOC=${TC_SYSROOT}/usr/bin/moc4
export OE_QMAKE_RCC=${TC_SYSROOT}/usr/bin/rcc

# Construct an ad-hoc poky mkspec
export OE_QMAKE_CC=arm-poky-linux-gnueabi-gcc
export OE_QMAKE_CXX=arm-poky-linux-gnueabi-g++
export OE_QMAKE_LINK=arm-poky-linux-gnueabi-g++
export OE_QMAKE_LINK_SHLIB=arm-poky-linux-gnueabi-g++
export OE_QMAKE_AR=arm-poky-linux-gnueabi-ar cqs
export OE_QMAKE_OBJCOPY=arm-poky-linux-gnueabi-objcopy
export OE_QMAKE_STRIP=arm-poky-linux-gnueabi-strip

${OE_QMAKE_QMAKE} QT_LIBINFIX=E -spec `cat ../CrossProjects/mkspec_path` QMAKE_CC="$OE_QMAKE_CC" QMAKE_CXX="$OE_QMAKE_CXX" QMAKE_LINK="$OE_QMAKE_LINK" QMAKE_LINK_SHLIB="$OE_QMAKE_LINK_SHLIB" QMAKE_AR="$OE_QMAKE_AR" QMAKE_OBJCOPY="$OE_QMAKE_OBJCOPY" QMAKE_STRING="$OE_QMAKE_STRIP"
make -j12


# TODO; make qmake handle these!
cp -r ModulOSC_scripts/src/${ARCH}/* ./build/${ARCH}
cp -r PyModulOSC/src/* ./build/${ARCH}
